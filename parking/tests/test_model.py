from django.test import TestCase
from space.models import Space, Booking
from usermanagement.models import User

class SpaceTestCase(TestCase):
    def setUp(self) -> None:
        user = User.objects.create(
            email = 'jane.doe@test.com',
            national_id = 12345678,
            first_name = 'Jane',
            last_name = 'Doe',
            is_active = True,
            is_admin = True,


        )
        Space.objects.create(
            owner = user,
            town = 'Nairobi',
            street_name = 'Kimathi Street',
            building = 'Corner House',
            charges_per_hour = 100.50,
            charges_per_day = 500.25,
            additional_payments = 45,
            number_of_parking_spaces=50,
        )

    def test_str_func(self):
        owner = User.objects.get(national_id=12345678)
        space = Space.objects.get(owner=owner)
        self.assertEqual(str(space), 'Corner House Kimathi Street')