from django.utils import timezone
from rest_framework.serializers import (ModelSerializer,)
from space.models import Reservation, Garage, Spot

class GarageSerializer(ModelSerializer):
    class Meta:
        model = Garage
        fields = [
            'owner', 
            'town', 
            'street_name', 
            'building', 
            'additional_payments', 
            'number_of_parking_spaces', 
            'description']

class ReservationSerializer(ModelSerializer):
    class Meta:
        model = Reservation
        extra_kwargs = {
            'customer': {'read_only': True}, 
            'check_in_time': {'read_only': True},
            'check_out_time': {'read_only': True},
        }
        fields = [
            'customer', 
            'spot', 
            'vehicle_registration', 
            'phone_number', 
            'parked', 
            'paid', 
            'check_in_time', 
            'check_out_time', 
            'amount_to_be_refunded', 
            'amount_owed', 
            'description']

    def create(self, validated_data):
        validated_data['customer'] = self.context['request'].user
        validated_data['check_in_time'] = timezone.now()
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data['check_out_time'] = timezone.now()
        return super().update(instance,validated_data)

class CarOwnerSerializer(ModelSerializer):
    class Meta:
        model = Reservation
        fields = ['description']

class SpotSerializer(ModelSerializer):
    class Meta:
        model = Spot
        fields = '__all__'