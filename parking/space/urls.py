from django.urls import path, include
from rest_framework.routers import DefaultRouter
from space.views import (GarageViewSet, ReservationViewSet, SpotViewSet, CancelReservationViewSet)

router = DefaultRouter()
router.register(r'garage', GarageViewSet)
router.register(r'reservation', ReservationViewSet)
router.register(r'spot', SpotViewSet)
router.register(r'cancel_reservation', CancelReservationViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
