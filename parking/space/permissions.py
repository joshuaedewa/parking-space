from rest_framework.permissions import BasePermission, SAFE_METHODS

class IsOwnerAdmin(BasePermission):
    """
    Grant permission if the user is the one that created
    the instance or the user is admin
    """
    def has_object_permission(self, request, view,obj):
        if request.method in SAFE_METHODS:
            return True
        return request.user == obj.owner or request.user.is_admin


class IsOwnerOrAdminToUpdate(BasePermission):
    """
    Permissions: Owner or Admin can only update the model using this view
    """
    def has_permission(self, request, view):
        return super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        print(request.method)
        METHODS_ALLOWED = ['UPDATE', 'PARTIAL_UPDATE']
        if request.method not in METHODS_ALLOWED:
            return False
        return request.user == obj.owner or request.user.is_staff