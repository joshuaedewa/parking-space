from django.db import models
from utils.models import BaseModel
from usermanagement.models import User

class Garage(BaseModel):
    """
    Contains information abaout the garage
    """
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    town = models.CharField(max_length=255)
    street_name = models.CharField( max_length=250)
    building = models.CharField(max_length=255)
    additional_payments = models.DecimalField(max_digits=5, decimal_places=2)
    number_of_parking_spaces = models.PositiveIntegerField()
    description  = models.TextField()

    def __str__(self):
        return f"{self.building} {self.street_name}"

class Spot(BaseModel):
    """
    Space to be reserved
    """
    HOURLYCHARGES = 'HC'
    DAILYCHARGES = 'DC'
    WEEKENDCHARGES = 'WC'

    PARKING_CHARGES = [
        (HOURLYCHARGES, 'Hourly Charges'),
        (DAILYCHARGES, 'Daily Charges'),
        (WEEKENDCHARGES, 'Weekend Charges')
    ]
    garage = models.ForeignKey(Garage, on_delete=models.CASCADE)
    spot_name = models.CharField(max_length=50)
    charges_per_hour = models.DecimalField(max_digits=5, decimal_places=2)
    charges_per_day = models.DecimalField(max_digits=5, decimal_places=2)
    booked = models.BooleanField(default=False)
    parking_charges = models.CharField(max_length=2, choices=PARKING_CHARGES, default=HOURLYCHARGES)
    description  = models.TextField()

    def __str__(self) -> str:
        return f"{self.garage}-{self.spot_name}"

class Reservation(BaseModel):
    """
    Model to Reserve a parking spot in given garage
    """
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    spot = models.ForeignKey(Spot, on_delete=models.CASCADE)
    vehicle_registration = models.CharField(max_length=10)
    phone_number = models.CharField(max_length=15)
    parked = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)
    check_in_time = models.DateTimeField(null=True, blank=True)
    check_out_time = models.DateTimeField(null=True, blank=True)
    amount_to_be_refunded = models.PositiveIntegerField(blank=True, null=True)
    amount_owed = models.PositiveIntegerField(blank=True, null=True)
    description = models.TextField()

    def __str__(self):
        return self.vehicle_registration
    