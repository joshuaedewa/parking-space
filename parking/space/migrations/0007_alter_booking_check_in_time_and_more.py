# Generated by Django 4.0.3 on 2022-03-19 20:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('space', '0006_alter_space_owner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='check_in_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='check_out_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
