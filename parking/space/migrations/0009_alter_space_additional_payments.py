# Generated by Django 4.0.3 on 2022-04-14 07:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('space', '0008_rename_additiona_payments_space_additional_payments_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='space',
            name='additional_payments',
            field=models.DecimalField(decimal_places=2, max_digits=5),
        ),
    ]
