from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie
from rest_framework.viewsets import ModelViewSet
from space.models import Garage, Reservation, Spot
from space.serializers import (GarageSerializer, 
                                ReservationSerializer, 
                                CarOwnerSerializer, 
                                SpotSerializer)
from .permissions import (IsOwnerAdmin, IsOwnerOrAdminToUpdate)

class GarageViewSet(ModelViewSet):
    permission_classes = [IsOwnerAdmin,]
    queryset = Garage.objects.all()
    serializer_class = GarageSerializer

    # Cache requested url for each user for 2 hours
    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60*2))
    def dispatch(self, request, *args, **kwargs ):
        return super(GarageViewSet, self).dispatch(request, *args, **kwargs)

class ReservationViewSet(ModelViewSet):
    permission_classes = [IsOwnerAdmin,]
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

    car_owner_methods = ['update', 'partial_update']

    def get_serializer_class(self):
        if self.action in self.car_owner_methods:
            return CarOwnerSerializer
        return super().get_serializer_class()


    # Cache requested url for each user for 2 hours
    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60*2))
    def dispatch(self, request, *args, **kwargs ):
        return super(ReservationViewSet, self).dispatch(request, *args, **kwargs)

class SpotViewSet(ModelViewSet):
    permission_classes = [IsOwnerAdmin,]
    queryset = Spot.objects.all()
    serializer_class = SpotSerializer

class CancelReservationViewSet(ModelViewSet):
    permission_classes = [IsOwnerOrAdminToUpdate,]
    queryset = Spot.objects.all()
    serializer_class = SpotSerializer