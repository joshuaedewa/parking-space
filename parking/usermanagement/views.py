from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie

from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from utils.permissions import IsOwnerOrAdminCreateOnly

from .serializers import UserSerializer
from .models import User

class UserViewSet(ModelViewSet):
    permission_classes = [IsOwnerOrAdminCreateOnly,]
    serializer_class = UserSerializer
    queryset = User.objects.all()
    

    # Cache requested url for each user for 2 hours
    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60*2))
    def dispatch(self, request, *args, **kwargs ):
        return super(UserViewSet, self).dispatch(request, *args, **kwargs)
    
    