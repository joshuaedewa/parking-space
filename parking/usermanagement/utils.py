def validate_customer(validated_data):
    validated_data.pop('is_admin', None)
    validated_data.pop('is_customer', None)
    validated_data.pop('is_active', None)
    return validated_data

def validated_admin(validated_data):
    validated_data.pop('is_customer', None)
    validated_data.pop('is_admin', None)
    validated_data.pop('is_active', None)
    return validated_data

def validate_officer(validated_data):
    validated_data.pop('is_admin', None)
    validated_data.pop('is_customer', None)
    validated_data.pop('is_active', None)
    return validated_data

def validate_user(validated_data):
    validated_data.pop('is_admin', None)
    validated_data.pop('is_customer', None)
    validated_data.pop('is_active', None)
    return validated_data