import random
import pdb

from django.core.management.base import BaseCommand, CommandError
from usermanagement.models import User


class Command(BaseCommand):
    help = 'Cleans the user table by removes national duplicates after it they were updated to unique'

    def add_arguments(self, parser) -> None:
        return super().add_arguments(parser)

    def handle(self, *args, **options):
        users = User.objects.all()
        for user in users:
            try:
                # pdb.set_trace()
                person = User.objects.get(pk=user.id)
                person.national_id = random.randint(1,10000000)
                person.save()
            except Exception as ex:
                raise CommandError(f'Somethings went wrong: {ex}')

        self.stdout.write(self.style.SUCCESS('Successfully updated national ids'))
