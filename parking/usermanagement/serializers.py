from rest_framework.serializers import ModelSerializer
from .models import User
from .utils import validate_user

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        extra_kwargs = {
            'password': {'write_only': True}, 
            'last_login': {'read_only': True},
            'is_officer': {'read_only': True}
        }
        fields = ['email', 'password', 'first_name', 'last_name', 'national_id', 'last_login', 'is_admin','is_staff', 'is_customer', 'is_officer', 'is_active']
        
    def create(self, validated_data):

        if validated_data['is_customer']:
            validated_data = validate_user(validated_data)
            user = User.objects.create_customer(**validated_data)
            return user
        if not validated_data['is_admin'] and not validated_data['is_customer']:
            validated_data = validate_user(validated_data)
            user = User.objects.create_staff(**validated_data)
            return user
        if validated_data['is_admin']:
            validated_data = validate_user(validated_data)
            user = User.objects.create_superuser(**validated_data)
            return user
        return super().create(validated_data)
        