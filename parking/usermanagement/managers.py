from django.contrib.auth.models import (BaseUserManager)

class UserManager(BaseUserManager):
    def create_user(self, email,first_name, last_name, national_id=None, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name, 
            last_name=last_name,
        )
        user.is_officer = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_customer(self, email, first_name, last_name, national_id, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        if not national_id:
            raise ValueError('Customer must have a national ID')
        user = self.model(
            email=self.normalize_email(email),
            national_id = national_id,
            first_name=first_name, 
            last_name=last_name,
        )
        user.is_customer = True
        user.is_active = False
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email,first_name, last_name, national_id=None, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            first_name=first_name, 
            last_name=last_name,
            password=password,
            national_id=None,
        )
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_staff(self, email,first_name, last_name, national_id=None, password=None):
        user = self.create_superuser(
            email,
            first_name=first_name, 
            last_name=last_name,
            password=password,
            national_id=None,
        )
        user.is_admin = False
        user.is_active = True
        user.save(using=self._db)
        return user