import pdb
from rest_framework.permissions import (BasePermission, SAFE_METHODS)

class IsOwnerOrReadOnly(BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute
    """

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.owner == request.user

class IsOwnerOrAdminCreateOnly(BasePermission):
    """
    ' Object-level permission to only allow owners and admin users to edit and read.
    ' And non-admin users to create only
    ' Assumes the model instance has an `owner` attribute
    """
    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):
        if request.method == 'POST':
            return True
        return obj.email == request.user or request.user.is_staff
